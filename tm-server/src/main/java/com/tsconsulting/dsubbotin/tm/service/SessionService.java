package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.ISessionService;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.AccessDeniedException;
import com.tsconsulting.dsubbotin.tm.model.Session;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    protected ISessionRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(ISessionRepository.class);
    }

    @NotNull
    @Override
    public Session open(
            @NotNull final String login,
            @NotNull final String password
    ) throws AbstractException {
        @NotNull final Session session = new Session();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            @NotNull final IPropertyService propertyService = new PropertyService();
            final int iteration = propertyService.getPasswordIteration();
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final String hash = HashUtil.salt(iteration, secret, password);
            if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
            session.setUserId(user.getId());
            session.setDate(new Date());
            session.setSignature(null);
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            sessionRepository.open(sign(session));
            sqlSession.commit();
        }
        return session;
    }

    @Override
    public boolean close(@NotNull final Session session) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            int res = sessionRepository.close(session);
            sqlSession.commit();
            return res > 0;
        }
    }

    @Override
    public void validate(
            @Nullable final Session session,
            @NotNull final Role role
    ) throws AbstractException {
        validate(session);
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            if (session == null) throw new AccessDeniedException();
            @NotNull final String userId = session.getUserId();
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) throw new UserNotFoundException();
            @NotNull final Role userRole = user.getRole();
            if (!userRole.equals(role)) throw new AccessDeniedException();
        }
    }

    @Override
    public void validate(@Nullable final Session session) throws AbstractException {
        checkSession(session);
        @Nullable final Session sessionTemp = session.clone();
        if (sessionTemp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        if (signatureSource == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sign(sessionTemp).getSignature();
        if (!signatureSource.equals(signatureTarget)) throw new AccessDeniedException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(sqlSession);
            if (sessionRepository.contains(session.getId()) == null) throw new AccessDeniedException();
        }
    }

    @NotNull
    @Override
    public User getUser(@NotNull final Session session) throws AbstractException {
        @NotNull final String userId = session.getUserId();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final Session session) {
        return session.getUserId();
    }

    @NotNull
    private Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = new PropertyService();
        final int iteration = propertyService.getSignatureIteration();
        @NotNull String secret = propertyService.getSignatureSecret();
        @Nullable final String signature = HashUtil.sign(iteration, secret, session);
        session.setSignature(signature);
        return session;
    }

    private void checkSession(@Nullable final Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getDate().toString())) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getSignature())) throw new AccessDeniedException();
    }

}
