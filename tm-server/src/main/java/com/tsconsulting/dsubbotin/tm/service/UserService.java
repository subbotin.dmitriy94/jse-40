package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.IUserService;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyLoginException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyPasswordException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserFoundException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    public IUserRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(IUserRepository.class);
    }

    @Override
    public void remove(@NotNull final User user) {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.remove(user);
            session.commit();
        }
    }

    @Override
    public void clear() {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.clear();
            session.commit();
        }
    }

    @Override
    @NotNull
    public List<User> findAll() throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable final List<User> users = userRepository.findAll();
            if (users == null) throw new UserNotFoundException();
            return users;
        }
    }

    @Override
    @NotNull
    public List<User> findAll(@NotNull final Comparator<User> comparator) throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable final List<User> users = userRepository.findAll();
            if (users == null) throw new UserNotFoundException();
            users.sort(comparator);
            return users;
        }
    }

    @Override
    @NotNull
    public User findById(@NotNull final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable final User user = userRepository.findById(id);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @Override
    @NotNull
    public User findByIndex(final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable final User user = userRepository.findByIndex(index);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @Override
    public void removeById(@NotNull final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.removeById(id);
            session.commit();
        }
    }

    @Override
    public void removeByIndex(final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable final User user = userRepository.findByIndex(index);
            if (user == null) throw new UserNotFoundException();
            userRepository.removeById(user.getId());
            session.commit();
        }
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password
    ) throws AbstractException {
        if (isLogin(login)) throw new UserFoundException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(getPasswordHash(password));
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.add(user);
            session.commit();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role
    ) throws AbstractException {
        if (isLogin(login)) throw new UserFoundException();
        checkPassword(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(getPasswordHash(password));
        user.setRole(role);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.add(user);
            session.commit();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role,
            @NotNull final String email
    ) throws AbstractException {
        if (isLogin(login)) throw new UserFoundException();
        checkPassword(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(getPasswordHash(password));
        user.setRole(role);
        user.setEmail(email);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.add(user);
            session.commit();
        }
        return user;
    }

    @Override
    public void addAll(@NotNull List<User> users) {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            for (@NotNull final User user : users) userRepository.add(user);
            session.commit();
        }
    }

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @Override
    public void removeByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.removeByLogin(login);
            session.commit();
        }
    }

    @Override
    public void setPassword(
            @NotNull final String id,
            @NotNull final String password
    ) throws AbstractException {
        checkId(id);
        checkPassword(password);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.setPassword(id, getPasswordHash(password));
            session.commit();
        }
    }

    @Override
    public void setRole(
            @NotNull final String id,
            @NotNull final Role role
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.setRole(id, role);
            session.commit();
        }
    }

    @Override
    public void updateById(
            @NotNull final String id,
            @NotNull final String lastName,
            @NotNull final String firstName,
            @NotNull final String middleName,
            @NotNull final String email
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.updateById(id, lastName, firstName, middleName, email);
            session.commit();
        }
    }

    @Override
    public boolean isLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            @Nullable final User user = userRepository.isLogin(login);
            return user != null;
        }
    }

    @Override
    public void lockByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.lockByLogin(login);
            session.commit();
        }
    }

    @Override
    public void unlockByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = getRepository(session);
            userRepository.unlockByLogin(login);
            session.commit();
        }
    }

    private void checkLogin(@NotNull final String login) throws EmptyLoginException {
        if (EmptyUtil.isEmpty(login)) throw new EmptyLoginException();
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkPassword(@NotNull final String password) throws EmptyPasswordException {
        if (EmptyUtil.isEmpty(password)) throw new EmptyPasswordException();
    }

    @NotNull
    private String getPasswordHash(@NotNull String password) {
        @NotNull final IPropertyService propertyService = new PropertyService();
        final int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        return HashUtil.salt(iteration, secret, password);
    }

}