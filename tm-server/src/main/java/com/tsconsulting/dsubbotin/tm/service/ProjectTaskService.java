package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectTaskService;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import lombok.RequiredArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@RequiredArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final ILogService logService;

    @Override
    public void bindTaskToProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            isEmpty(projectRepository, taskRepository, userId, projectId, taskId);
            taskRepository.bindTaskToProjectById(userId, projectId, taskId);
            session.commit();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            isEmpty(projectRepository, taskRepository, userId, projectId, taskId);
            taskRepository.unbindTaskById(userId, taskId);
            session.commit();
        }
    }

    @Override
    @NotNull
    public List<Task> findAllTasksByProjectId(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository.findByUserIdAndProjectId(userId, id) == null) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(userId, id);
            if (tasks == null) throw new TaskNotFoundException();
            return tasks;
        }
    }

    @Override
    public void removeProjectById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository.findByUserIdAndProjectId(userId, id) == null) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.removeAllTaskByProjectId(userId, id);
            projectRepository.removeByUserIdAndProjectId(userId, id);
            session.commit();
        }
    }

    @Override
    public void removeProjectByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @Nullable final Project project = projectRepository.findByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final String projectId = project.getId();
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByUserIdAndProjectId(userId, projectId);
            session.commit();
        }
    }

    @Override
    public void removeProjectByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable final Project project = projectRepository.findByName(userId, name);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final String projectId = project.getId();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByName(userId, name);
            session.commit();
        }
    }

    @Override
    public void removeAllProject(@NotNull String userId) throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @Nullable final List<Project> projects = projectRepository.findAllByUserId(userId);
            if (projects == null) throw new ProjectNotFoundException();
            for (Project project : projects) taskRepository.removeAllTaskByProjectId(userId, project.getId());
            projectRepository.clearById(userId);
            session.commit();
        }
    }

    private void isEmpty(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(projectId)) throw new EmptyIdException();
        if (EmptyUtil.isEmpty(taskId)) throw new EmptyIdException();
        if (projectRepository.findByUserIdAndProjectId(userId, projectId) == null) throw new ProjectNotFoundException();
        if (taskRepository.findByUserIdAndTaskId(userId, taskId) == null) throw new TaskNotFoundException();
    }

}
