package com.tsconsulting.dsubbotin.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
public final class Session extends AbstractEntity implements Cloneable {

    @NotNull
    private Date date;

    @NotNull
    private String userId;

    @Nullable
    private String signature;

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }


}
